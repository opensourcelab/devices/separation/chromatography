LabPyUi to-do
===============

- current light int. in handler / all data arrays in handler
- only one handler (*args, **kwargs)
- clean worker
- split mainwindow.py
- csv export: separate module , annoation of first entry gets metainfo in 
standardized format -  datetime: in ISO | description: |  device: | users: | project_name:  


## filerequester:
 one single requester



## User management
  

## slider
 - 99 % bug
 - temp and speed slider settings bug

## Timer
 - timer expamples

## step editor

## Alphanumeric
 - set unit (once for all data)
 - multiple rows and columns

## matplot
arrow plot on data point

## WidgetRack
  example: weighing

## installer / requirements file
 - pip freeze > requirements.txt


 # worker
 - pause task
 - terminate only at the end
 
# controller 
    - color switch button
    - sending data to arduino

## sila workbench
 - generic interface to sila services
 - auto detection of services
 - table that connects feature to widget
    - balance
    - pump
    - thermometer
    - pH meter
    - incubator
    - thermostate
    - shaker
    - PCR 

 ## data logger
          - det. logger
          - pump logger
          - 3D display
          - pump simulator
          - temperature logger
          - 3D display of real data
          - python Editor 
  automatic generation of filenames (s. pHmeter)
 file storage on SiLA server

## photoreactor
 - all centered
 - numeric displays for light intensity
 - light handler labplotlib ui ref. 
 - config 
 - celsius by default
 - main window tabs bigger
 - matplotlib
 - mv everything to ctrl-slider
 - mv to class
 - QWorker for all timers ... (s. pyqt signal slot)
 - annoation (text, arrow)
 - documentation
 - logging (warnings, errors)

## colony counter
change image parameter (saturation, blur, colour ...)

# metadata menu
 - user management
 - login widget: username / Project
 - description
 - user selector (with multiple users)

## updated manager:
 - check updates
 - show version number and CHANGELOG (else show current version)
 - confirm update 

## AnIML support (as export format and generic viewer)


generic plot based on matplotlib and qtplot
integration of plot widget in instrument rack
generic icons 
generic intensity regulator

check: app settings save routine

larger tabs

colour shades
rename methods in plot

fix settings 
fix timer widget
fix matplot widget - rename methods

stirrer -> rotational_speed
cytomat 
step editor
add matplot time window plot (reduce data)

dark theme config 
autosave of log

separation of Lab_Application and main window (to use different main windows)
SiLA menue:
    connection settings
    feature view (html view generated for XSTL)

micro time based scheduler:
 - loop over time events 
 - issue command, based on time event 


peak width sorting 

signal gen sim
signal sim:
- droplet sim

overlay plots
statistic view

arduino programmer

implementing oszi mode (monitor, threshold)

combining time data with measurements ...

optimise user interactions
name save std data canonical filename
better serial interface handling (define interface to monitor)

voltage calibration (display)

stat view - histogram view

recording indicator

sort setting feedback

video4 linux viewer
openCV video stream viewer

redesign oszi widget (group widget)

oszilloscope
wave generator
dispenser

incubator

pH 
refractive index measurement


pump

pump slider connect to events

device control process language

pump algorithm 

pumpprofile class


process language

droplet calculator:
basic: radius, volume, surface
concentration: num of water, num of molecules at given concentration, diffusion times,
viscosity ?

droplet analyzer:
counting droplets and size distribution histogram
std. dev. of sizes ....

visible when added -> view / tools menu

LabTimer Widget

Wave generator 
- QT phonon based

labworkbench:
New Project dir ...

python shell calculator
voltage devider calculator


LARA Process editor


Labview
max/min
peak width
 

gaussian_kde kernel d? e? (s. numpy)

2D histogram (s. plotly)
Voltage vs size / scatter plot with gathing

